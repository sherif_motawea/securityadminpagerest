package edu.mum.service;

import java.util.List;

import edu.mum.domain.Authority;

public interface AuthorityService {
	
	public List<Authority> findAll();

}
