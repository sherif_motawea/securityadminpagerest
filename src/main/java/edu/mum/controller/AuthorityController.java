package edu.mum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.mum.domain.Authority;
import edu.mum.service.AuthorityService;

@RestController
@RequestMapping({"/authorities"})
public class AuthorityController {
	
	@Autowired
	AuthorityService authorityService;
	
	@RequestMapping
	public @ResponseBody List<Authority> findAll(){
		return authorityService.findAll();
	}

}
