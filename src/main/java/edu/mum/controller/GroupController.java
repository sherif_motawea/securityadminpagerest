package edu.mum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.mum.domain.Group;
import edu.mum.service.GroupService;

@RestController
@RequestMapping({"/groups"})
public class GroupController {
	
	@Autowired
	GroupService groupService;
	
	@RequestMapping
	public @ResponseBody List<Group> findAll(){
		return groupService.findAll();
	}

}
